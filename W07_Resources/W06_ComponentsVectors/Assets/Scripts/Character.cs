﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {

    // Method 1: Reference Rigidbody2D through script
    // - Not shown in Inspector
    Rigidbody2D rb;

    // Method 2: Reference Rigidbody2D through script
    // - Shown in Inspector
    public Rigidbody2D rb2;

    // Handle movement speed of Character
    // - Can be adjusted through Inspector while in Play mode
    public float speed;

    // Handles jump speed of Character
    public float jumpForce;             // How high the character will Jump
    public bool isGrounded;             // Is the player touching the ground?
    public LayerMask isGroundLayer;     // What is the Ground? Player can only jump on things that are on the "Ground" layer  
    public Transform groundCheck;       // Used to figure out if the player is touching the ground
    public float groundCheckRadius;     // Size of circle around empty GameObject

    // Handles animations
    public Animator anim;

    // Handles flipping Character
    public bool isFacingLeft;

    // Handles Prefab instantiation (aka creation)
    public Transform projectileSpawnPoint;
    public Projectile projectile;
    public float projectileForce;

    // Handles amount of lives
    int _lives;

    // Use this for initialization
    void Start()
    {
        // Method 1: Reference Rigidbody through script
        rb = GetComponent<Rigidbody2D>();
        
        // Checks if Component exists
        if (!rb)
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.LogWarning("No Rigidbody2D found on GameObject");
        }

        // Checks if Component exists
        if (!rb2)
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.LogWarning("No Rigidbody2D found on GameObject");
        }
        
        // Check if speed was set to something not 0
        if (speed == 0)
        {
            // Assign a default value if one is not set in the Inspector
            speed = 5.0f;

            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.Log("Speed was not set. Defaulting to " + speed);
        }
        
        // Check if speed was set to something not 0
        if (jumpForce == 0)
        {
            // Assign a default value if one is not set in the Inspector
            jumpForce = 5.0f;
            
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.Log("Jump Force was not set. Defaulting to " + jumpForce);
        }

        // Checks if GroundCheck GameObject is connected
        if (!groundCheck)
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.LogError("No Ground Check found on GameObject");
        }

        // Check if speed was set to something not 0
        if (groundCheckRadius == 0)
        {
            // Assign a default value if one is not set in the Inspector
            groundCheckRadius = 0.2f;

            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.Log("Ground Check Radius was not set. Defaulting to " + groundCheckRadius);
        }

        // Reference Component through script
        anim = GetComponent<Animator>();

        // Checks if Component exists
        if (!anim)
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.LogError("No Animator found on GameObject");
        }

        // Checks if ProjectileSpawnPoint GameObject is connected
        if (!projectileSpawnPoint)
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.LogError("No Projectile SpawnPoint found on GameObject");
        }

        // Checks if Projectile GameObject is connected
        if (!projectile)
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.LogError("No Projectile found on GameObject");
        }

        // Check if speed was set to something not 0
        if (projectileForce == 0)
        {
            // Assign a default value if one is not set in the Inspector
            projectileForce = 7.0f;

            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.Log("Projectile Force was not set. Defaulting to " + projectileForce);
        }

    }
	
	// Update is called once per frame
	void Update () {
        
        // Check if groundCheck GameObject is touching something tagged as Ground or Platform
        // - Can change groundCheckRadius to a smaller value if you need more precision or if the sizing of the Character is small
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius,
            isGroundLayer);

        // Checks if Left (or a) or Right (or d) is pressed
        // - "Horizontal" must exist in Input Manager (Edit-->Project Settings-->Input)
        // - Returns -1(left), 1(right), 0(nothing)
        // - Use GetAxis for value -1-->0-->1 and all decimal places. (Gradual change in values)
        float moveValue = Input.GetAxisRaw("Horizontal");

        // Check if "Jump" button was pressed
        // - "Jump" must exist in Input Manager (Edit-->Project Settings-->Input)
        // - Configuration can be changed later
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.Log("Jump");

            // Vector2.up --> new Vector2(0,1);
            // Vector2.down --> new Vector2(0,-1);
            // Vector2.right --> new Vector2(1,0);
            // Vector2.left --> new Vector2(-1,0);

            // Applies a force in the UP direction
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }

        // Check if Left Control or Left Click was pressed
        // - "Fire1" must exist in Input Manager (Edit-->Project Settings-->Input)
        // - Configuration can be changed later
        if (Input.GetButtonDown("Fire1"))
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.Log("Pew pew");

            // Tells Animator to transition to another Clip
            // - Parameter 'Attack' must be created in Animator window
            anim.SetTrigger("Attack");

            // Create Projectile and add to Scene
            // - Thig to create (projectile gameObejct from Project Panel)
            // - Location to spawn (projectileSpawnPoint)
            // - Rotation of spawned gameObject (projectileSpawnPoint)
            Projectile temp = Instantiate(projectile,
                projectileSpawnPoint.position,
                projectileSpawnPoint.rotation) as Projectile;

            /*temp.GetComponent<Rigidbody2D>().velocity = 
                new Vector2( projectileForce, 0);
            */

            /*temp.GetComponent<Rigidbody2D>().velocity = 
                Vector2.right * projectileForce;
            */

            //temp.GetComponent<Projectile>().speed = projectileForce;

            if(!isFacingLeft)
                // Apply movement speed to projectile that is spawned
                temp.speed = projectileForce;
            else
                temp.speed = -projectileForce;

        }

        // Check if Left Control was pressed
        // - Tied to key and cannot be changed
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.Log("Pew pew");
        }

        // Move Character using Rigidbody2D
        // - Uses moveValue from GetAxis to move left or right
        rb.velocity = new Vector2(moveValue * speed, rb.velocity.y);

        // Tells Animator to transition to another Clip
        // - Parameter 'MoveValue' must be created in Animator window
        anim.SetFloat("MoveValue", Mathf.Abs(moveValue));

        // Tells Animator to transition to another Clip
        // - Parameter 'Grounded' must be created in Animator window
        anim.SetBool("Grounded", isGrounded);

        // Check if Character should flip and look left or right
        if ((moveValue < 0 && !isFacingLeft)|| (moveValue > 0 && isFacingLeft))
            // Call function to flip Character
            flip();
	}

    // Function that will flip Character look left or right
    void flip()
    {

        // Method 1: Toggle isFacingLeft variable
        isFacingLeft = !isFacingLeft;

        // Method 2: Toggle isFacingLeft variable
        /*if (isFacingLeft)
            isFacingLeft = false;
        else
            isFacingLeft = true;
        */

        // Make a copy of old scale value
        Vector3 scaleFactor = transform.localScale;

        // Flip scale of 'x' variable
        scaleFactor.x *= -1; // scaleFactor.x = -scaleFactor.x;

        // Update scale to new flipped value
        transform.localScale = scaleFactor;
    }

    // Give acces to private data (instance variables)
    // - Not needed if variable is public
    public int lives
    {
        get { return _lives; }
        set {
            _lives = value;
            Debug.Log("Lives changed to " + _lives);
        }
    }
}
