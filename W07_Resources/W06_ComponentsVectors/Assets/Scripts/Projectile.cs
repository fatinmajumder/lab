﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    // Used to tell gameObject how fast to move
    public float speed;

    // Used to tell gameObject how long to live for
    public float lifeTime;

	// Use this for initialization
	void Start () {

        // Check if lifeTime was set to something not 0
        if (lifeTime == 0)
        {
            // Assign a default value if one is not set in the Inspector
            lifeTime = 1.0f;

            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.Log("LifeTime was not set. Defaulting to " + lifeTime);
        }

        // Take Rigidbody2D component and change its velocity to value passed
        GetComponent<Rigidbody2D>().velocity =
                new Vector2(speed, 0);

        /*GetComponent<Rigidbody2D>().velocity = 
                Vector2.right * speed;
        */

        // Destroy gameObject after 'lifeTime' seconds
        Destroy(gameObject, lifeTime);
    }

    // Checks for Collisions
    // - At least one GameObject involved in Collision must have a Rigidbody2D attached
    // - Will not work for Colliders set to "Is Trigger"
    void OnCollisionEnter2D(Collision2D c)
    {
        // Check if GameObject projectile hits is not named "Player"
        // - Stops projectile from being destroyed if it hits a "Player" GameObject
        if (c.gameObject.tag != "Player")
        {
            // Remove GameObject (Projectile) from Scene
            Destroy(gameObject);
        }
    }
}
