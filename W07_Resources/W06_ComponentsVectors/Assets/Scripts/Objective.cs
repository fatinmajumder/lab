﻿using UnityEngine;
using System.Collections;

public class Objective : MonoBehaviour {

    // Used to keep track of Collectibles
    public int numberOfCollectibles;

    // Used to keep track of Collectibles left
    public GameObject[] allCollectibles;

    // Used to keep a reference to Player GameObject
    public GameObject player;

    // Used to keep a reference to finishLine GameObject
    public GameObject finishLine;

	// Use this for initialization
	void Start () {

        // Looks through entire Scene for GameObjects tagged as "Collectible"
        // - Returns everything active that is tagged as "Collectible"
        // - Typically found in order they were added to Scene
        // - Should be used sparingly and only in Start() or Awake()
        allCollectibles = GameObject.FindGameObjectsWithTag("Collectible");

        // Stores number of GameObjects
        numberOfCollectibles = allCollectibles.Length;

        // Looks through Scene for a GameObject named "Mario" in Hierarchy
        // - GameObject must be active 
        player = GameObject.Find("Mario");

        // Looks through Scene for a GameObject tagged "Player"
        // - GameObject must be active
        player = GameObject.FindGameObjectWithTag("Player");

        // Looks through Scene for a GameObject named "Objective" in Hierarchy
        // - GameObject must be active 
        finishLine = GameObject.Find("Objective");
    }

    // Update is called once per frame
    void Update () {
	    
        // Only runs if both "Player" and "Objective" were found
        if(player && finishLine)
        {
            // Calculate distance between the two GameObjects
            float distanceToFinish = Vector2.Distance(player.transform.position,
                finishLine.transform.position);

            /*distanceToFinish = 
                (finishLine.transform.position - player.transform.position).magnitude;
                */
                
            // Print distance to Console
            //Debug.Log("Distance Remaining: " + distanceToFinish);
        }

	}
}
