﻿using UnityEngine;
using System.Collections;

// Automatically adds Components when Script is added to GameObject
[RequireComponent(typeof(Rigidbody2D))]     // Rigidbody2D is added if not already added
[RequireComponent(typeof(BoxCollider2D))]   // BoxCollider2D is added if not already added
public class ExtraLife : MonoBehaviour {

    // Used to a reference to a Rigidbody2D or BoxCollider2D
    Rigidbody2D rb;
    BoxCollider2D bc;

	// Use this for initialization
	void Start () {

        // Grab Components to keep a reference for use later on
        rb = GetComponent<Rigidbody2D>();
        bc = GetComponent<BoxCollider2D>();

        // Change Rigidbody2D variables through Script
        rb.gravityScale = 0;
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;

        // Change BoxCollider2D variables through Script
        bc.isTrigger = true;
    }

    // Checks for Collisions
    // - At least one GameObject involved in Collision must have a Rigidbody2D attached
    // - Only works for Colliders set to "Is Trigger"
    void OnTriggerEnter2D(Collider2D c)
    {
        // Check if GameObject projectile hits is named "Player"
        if (c.gameObject.tag == "Player")
        {
            // Grab Script attached to Player GameObject
            Character cc = c.gameObject.GetComponent<Character>();

            // Check if Script (Character.CS) was attached to Player
            if(cc)
            {
                // Increas "lives" variable stored in Character Script
                cc.lives++;
            }

            // Remove GameObject Player collided with
            Destroy(gameObject);
        }
    }
}
